/*
Створити клас Car , Engine та Driver.
Клас Driver містить поля - ПІБ, стаж водіння.
Клас Engine містить поля – потужність, виробник.
Клас Car містить поля – марка автомобіля, клас автомобіля, вага, водій типу Driver, мотор типу Engine. 
Методи start(), stop(), turnRight(), turnLeft(), які виводять на друк: "Поїхали", "Зупиняємося", 
"Поворот праворуч" або "Поворот ліворуч". А також метод toString(), який виводить повну інформацію про 
автомобіль, її водія і двигуна.

Створити похідний від Car клас - Lorry (вантажівка), що характеризується також вантажопідйомністю кузова.
Створити похідний від Car клас - SportCar, який також характеризується граничною швидкістю.
*/

class Driver {
    constructor(fName, experience) {
        this.fullName = fName;
        this.expirience = experience;
    }

    toStr() {
        return `ПІБ - ${this.fullName}, водійський стаж - ${this.expirience} р.`;
    }
}

class Engine {
    constructor(ePower, producer) {
        this.power = ePower;
        this.company = producer;
    }

    toStr() {
        return `потужність - ${this.power} к.с., виробник - ${this.company}`;
    }
}

class Car {
    constructor(cBrand, cClass, cWeight, cDriver, cEngine) {
        this.brand = cBrand;
        this.carClass = cClass;
        this.weight = cWeight;
        this.driver = cDriver;
        this.engine = cEngine;
    }

    start() {
        alert('Поїхали');
    }

    stop() {
        alert('Зупиняємося');
    }

    turnRight() {
        alert('Поворот праворуч');
    }

    turnLeft() {
        alert('Поворот ліворуч');
    }

    toString() {
        console.log(`Марка автомобіля: ${this.brand}
Клас автомобіля: ${this.carClass}
Вага: ${this.weight} кг
Двигун: ${this.engine.toStr()}
Водій: ${this.driver.toStr()}`);
    }
}

class Lorry extends Car {
    constructor(cBrand, cClass, cWeight, cDriver, cEngine, lCarying) {
        super(cBrand, cClass, cWeight, cDriver, cEngine)
        this.carying = lCarying;
    }
}

class SportCar extends Car {
    constructor(cBrand, cClass, cWeight, cDriver, cEngine, maxSpeed) {
        super(cBrand, cClass, cWeight, cDriver, cEngine)
        this.speed = maxSpeed;
    }
}

let newDriver = new Driver('Грінченко Віталій Миколайович', 15),
    newEngine = new Engine(150, 'Volkswagen'),
    newEngine2 = new Engine(750, 'Man'),
    newEngine3 = new Engine(400, 'Ferarri'),
    newCar = new Car('Skoda', 'liftback', 1400, newDriver, newEngine),
    newLorry = new Lorry('Man', 'truck', 11200, newDriver, newEngine2, 22),
    newSport = new SportCar('Ferarri', 'sport', 1600, newDriver, newEngine3, 330);