// Порожній прямокутник

for (let i = 0; i < 1; i++) {
    for (let j = 0; j < 10; j++) {
        document.write("&nbsp;&nbsp;");
    }
    document.write("*&nbsp;&nbsp;");

    for (let j = 0; j < 30; j++) {
        document.write("*&nbsp;&nbsp;");
    }
    document.write("<br/>");
}

for (let i = 0; i < 20; i++) {
    for (let j = 0; j < 10; j++) {
        document.write("&nbsp;&nbsp;");
    }
    document.write("*");

    for (let j = 0; j < 29; j++) {
        document.write("&nbsp;&nbsp;&nbsp;&nbsp;");
    }
    document.write("&nbsp;&nbsp;*");
    
    document.write("<br/>");
    
}

for (let i = 0; i < 1; i++) {
    for (let j = 0; j < 10; j++) {
        document.write("&nbsp;&nbsp;");
    }
    document.write("*&nbsp;&nbsp;");

    for (let j = 0; j < 30; j++) {
        document.write("*&nbsp;&nbsp;");
    }
    document.write("<br/>");
}

document.write("<br/>");


//Трикутник

for (let i = 0; i < 20; i++) {
    for (let j = 0; j < 10; j++) {
        document.write("&nbsp;&nbsp;");
    }

    for (let j = 1 + i; j < 20; j++) {
        document.write("&nbsp;&nbsp;&nbsp;");
    }

    for (let j = 1 + i; j > 0; j--) {
        document.write("*&nbsp;&nbsp;&nbsp;&nbsp;");
    }
    document.write("<br/>");

}

document.write("<br/>");


// Ромб

for (let i = 0; i < 20; i++) {
    for (let j = 0; j < 10; j++) {
        document.write("&nbsp;&nbsp;");
    }

    for (let j = 1 + i; j < 20; j++) {
        document.write("&nbsp;&nbsp;&nbsp;");
    }

    for (let j = 1 + i; j > 0; j--) {
        document.write("*&nbsp;&nbsp;&nbsp;&nbsp;");
    }
    document.write("<br/>");
}

document.write("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
for (let i = 1; i < 22; i++) {
    document.write("*&nbsp;&nbsp;&nbsp;&nbsp;");
}
document.write("<br/>");

for (let i = 21; i > 1; i--) {
    for (let j = 10; j > 0; j--) {
        document.write("&nbsp;&nbsp;");
    }

    for (let j = 21 - i; j > 0; j--) {
        document.write("&nbsp;&nbsp;&nbsp;");
    }

    for (let j = 21 - i; j < 20; j++) {
        document.write("*&nbsp;&nbsp;&nbsp;&nbsp;");
    }
    document.write("<br/>");
}