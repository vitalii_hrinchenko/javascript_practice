let m;
let n;

do {
    m = parseInt(prompt('Введіть число m'));
    n = parseInt(prompt('Введіть число n (n має бути більше за m)'));
    if (n <= m) {
        alert('Виникла помилка! Число n має бути більшим за m');
    }
} while (n <= m);

for (let i = m; i <= n; i++) {
    let flag = 0;

    for (let j = 2; j < i; j++) {
        if (i % j == 0) {
            flag = 1;
            break;
        }
    }

    if (i > 1 && flag == 0) {
        console.log(i);
    }
}