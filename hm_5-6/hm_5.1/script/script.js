const empty = {};
const full = {
    property1: 'full',
    property2: 'object'
};

function isEmpty(obj) {
    for (let prp in obj) {
        return false;
    }
    return true;
}

console.log(empty);
console.log(isEmpty(empty));

console.log(full);
console.log(isEmpty(full));