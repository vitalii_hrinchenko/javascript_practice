function Human(name, sname, age, sex, salary) {
    this.name = name;
    this.sname = sname;
    this.age = age;
    this.sex = sex;
    this.salary$ = salary;
};
    //Метод екземпляру
Human.prototype.salaryH = function () {
    return this.salary$ * 37;
};

    //Метод класу
Human.sortByage = function (arr, order) {
    if (order == "up") {
        arr.sort((a, b) => a.age > b.age ? 1 : -1);
    } else if (order == "down") {
        arr.sort((a, b) => a.age < b.age ? 1 : -1);
    } else {
        console.error("Помилка!");
    }
};

const humans = [new Human('Іван', 'Іваненко', 34, 'чоловік', 700), new Human('Петро', 'Петренко', 29, 'чоловік', 1200), new Human('Катерина', 'Карпенко', 47, 'жінка', 900), new Human('Софія', 'Зенченко', 21, 'жінка', 1700)];

Human.sortByage(humans, 'up');
console.log(humans);