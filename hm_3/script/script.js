const styles = ['Джаз', 'Блюз'];
console.log(styles);

styles.push('Рок-н-Рол');
console.log(styles);

// Якщо елементів в масиві парна кількість, то посередині масиву вставляємо новий елемент 'Класика1'
if (styles.length % 2 === 0) {
    styles.splice((styles.length / 2), 0, 'Класика');
}

// Якщо елементів в масиві непарна кількість, то елемент посередині замінюємо на елемент 'Класика'
else if (styles.length % 2 != 0) {
    styles.splice(Math.floor(styles.length / 2), 1, 'Класика');
}

console.log(styles);

const ind0 = styles.shift();
console.log(ind0);

styles.splice(0, 0, 'Реп', 'Реггі');
console.log(styles);

/* Пробував ще зробити так:
styles.unshift('Реп', 'Реггі');
console.log(styles);
але консоль чомусь некоректно видає результат
*/