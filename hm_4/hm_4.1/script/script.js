function map(fn, arr) {
    const newArr = [];
    for (let i = 0; i < arr.length; i++) {
        newArr.push(fn(arr[i]));
    }
    return newArr;
}

function mult (a) {
    return a * 2;
}

const arr = new Array(10);
for (let i = 0; i < arr.length; i++) {
    arr[i] = Math.floor(Math.random() * 100);
}
console.log(`Початковий масив: ${arr}`);

console.log(`Змінений масив: ${map(mult, arr)}`);